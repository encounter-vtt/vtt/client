# EncounterVTT Client

This is the new client, replacing `wgpu-rs` with `druid-rs`.

### Why the switch?

`wgpu-rs` is a great library, and it may still be used in the future by this project. However, for the sake of faster
feature iteration, `druid` is more ideal. It provides a retained GUI context, had many of the interface details we would
need to implement ourselves, and has provided solid foundational tools to create our own features. 