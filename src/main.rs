use std::iter::Iterator;

use druid::{
    AppLauncher, BoxConstraints, Color, Data, Env, Event, EventCtx, LayoutCtx, LifeCycle,
    LifeCycleCtx, LocalizedString, MouseButton, PaintCtx, Point, Rect, RenderContext, Size,
    UpdateCtx, Widget, WidgetExt, WindowDesc,
};

mod widgets;

use crate::widgets::grid::Grid;
use crate::widgets::panes;

const WINDOW_TITLE: LocalizedString<ZstState> = LocalizedString::new("Druid: Circle of Encounter");

#[derive(Clone, Data)]
struct ZstState;

pub fn main() {
    // describe the main window
    let main_window = WindowDesc::new(make_root_widget)
        .title(WINDOW_TITLE)
        .window_size((1920.0, 1300.0));

    // start the application
    AppLauncher::with_window(main_window)
        .launch(ZstState)
        .expect("Failed to launch application");
}

fn make_root_widget() -> impl Widget<ZstState> {
    let grid_bounds = BoxConstraints::new(Size::new(600.0, 600.0), Size::new(1000.0, 1000.0));

    panes::Window::default().with_pane(Grid::default(), "This is a grid".into(), grid_bounds)
}

// #[cfg(not(feature = "canvas"))]
// fn make_root_widget() -> impl Widget<ZstState> {
//     // let grid = panes::new_pane(Point { x: 20.0, y: 20.0 }, "This is a grid".into(), Size { width: 800.0, height: 800.0 }, Grid::default());
//
//     // Canvas::new()
//     //    .with_child(CanvasWrap::new(grid, |d| Point::ZERO))
//
//     panes::new_pane(Point { x: 20.0, y: 20.0 }, "This is a grid".into(), Size { width: 800.0, height: 800.0 }, Grid::default())
// }
