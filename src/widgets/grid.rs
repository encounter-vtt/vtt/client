use druid::{
    BoxConstraints, Color, Env, Event, EventCtx, LayoutCtx, LifeCycle, LifeCycleCtx, MouseButton,
    PaintCtx, Point, Rect, RenderContext, Size, UpdateCtx, Widget,
};

use crate::ZstState;

#[derive(Default)]
pub struct Grid {
    offset: Point,
    dragging: Option<(Point, Point)>,
}

impl Widget<ZstState> for Grid {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, _data: &mut ZstState, _env: &Env) {
        match event {
            Event::MouseUp(e) => {
                if e.button == MouseButton::Left {
                    self.dragging = None;
                }
            }
            Event::MouseDown(e) => {
                if e.button == MouseButton::Left && self.dragging.is_none() {
                    self.dragging = Some((e.pos, self.offset));
                }
            }
            Event::MouseMove(e) => {
                if let Some((origin, old_offset)) = self.dragging {
                    self.offset = old_offset + (e.pos - origin);
                    ctx.request_paint();
                }
            }
            _ => {}
        }
    }

    fn lifecycle(
        &mut self,
        _ctx: &mut LifeCycleCtx,
        _event: &LifeCycle,
        _data: &ZstState,
        _env: &Env,
    ) {
    }

    fn update(&mut self, ctx: &mut UpdateCtx, _old_data: &ZstState, _data: &ZstState, _env: &Env) {
        ctx.request_paint();
    }

    fn layout(
        &mut self,
        _ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        _data: &ZstState,
        _env: &Env,
    ) -> Size {
        bc.max()
    }

    fn paint(&mut self, ctx: &mut PaintCtx, _data: &ZstState, _env: &Env) {
        let bg = ctx.region().bounding_box();
        ctx.fill(bg, &Color::rgb8(42, 11, 61));

        let Size { width, height } = ctx.size();

        let (x_offset, y_offset) = calc_grid_offset(self.offset);

        for y in (y_offset..height as i64).step_by(40) {
            for x in (x_offset..width as i64).step_by(40) {
                let origin = Point {
                    x: x as f64,
                    y: y as f64,
                };
                let size = Size {
                    width: 40_f64,
                    height: 40_f64,
                };
                let shape = Rect::from_origin_size(origin, size);

                ctx.stroke(shape, &Color::grey8(188), 4_f64);
            }
        }
    }
}

fn calc_grid_offset(offset: Point) -> (i64, i64) {
    if offset == Point::ZERO {
        return (0, 0);
    }

    let Point {
        x: x_offset,
        y: y_offset,
    } = offset;

    (((x_offset as i64 % 40) - 40), ((y_offset as i64 % 40) - 40))
}
