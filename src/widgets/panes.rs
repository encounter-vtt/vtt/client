//! A window-managing widget. Effectively a Window Manager a la x11 for Druid
use crate::ZstState;
use druid::kurbo::Rect;
use druid::widget::Label;
use druid::{
    BoxConstraints, Color, Data, Env, Event, EventCtx, LayoutCtx, LifeCycle, LifeCycleCtx,
    MouseButton, PaintCtx, Point, RenderContext, Size, UpdateCtx, Widget, WidgetPod,
};
use std::collections::{HashMap, VecDeque};

const PANE_TITLE_ORIGIN: Point = Point::new(8.0, 4.0);
const PANE_INNER_ORIGIN: Point = Point::new(8.0, 28.0);

const ERROR_MISSING_PANE_BOUNDS: &str = "Pane does not have bounds";
const ERROR_ARTEFACT_FROM_DELETED_PANE: &str = "Deleted pane has left over artefacts";
const ERROR_PANE_MISSING_RELATED_VALUE: &str = "Live pane is missing related data";

#[derive(Default)]
pub struct Window {
    // --- Meta state - affects the window as a whole
    inc: usize,
    pane_mut: PaneMutation,
    // --- Pane state - window-owned state that affects panes
    panes: HashMap<usize, Pane>,
    bounds: HashMap<usize, (Rect, BoxConstraints)>,
    drag_zone: HashMap<usize, Rect>,
    z_order: VecDeque<usize>,
}

pub struct Pane {
    inner: WidgetPod<ZstState, Box<dyn Widget<ZstState>>>,
    title: WidgetPod<ZstState, Label<ZstState>>,
}

#[derive(Copy, Clone)]
enum PaneMutation {
    Dragging(usize, Point),
    Resizing(usize, Point, ResizeDir),
    None,
}

impl Default for PaneMutation {
    fn default() -> Self {
        PaneMutation::None
    }
}

impl PaneMutation {
    fn is_none(&self) -> bool {
        match self {
            PaneMutation::None => true,
            _ => false,
        }
    }
}

impl Window {
    pub fn with_pane(
        mut self,
        widget: impl Widget<ZstState> + 'static,
        title: String,
        constraints: BoxConstraints,
    ) -> Self {
        self.new_pane(widget, title, constraints);
        self
    }

    pub fn add_pane(
        &mut self,
        ctx: &mut EventCtx,
        widget: impl Widget<ZstState> + 'static,
        title: String,
        constraints: BoxConstraints,
    ) {
        self.new_pane(widget, title, constraints);
        ctx.children_changed();
    }

    fn new_pane(
        &mut self,
        widget: impl Widget<ZstState> + 'static,
        title: String,
        constraints: BoxConstraints,
    ) {
        let id = self.inc;
        self.inc += 1;

        let pane_bc = {
            let inner_min = constraints.min();
            let inner_max = constraints.max();

            let min = Size::new(inner_min.width + 16.0, inner_min.height + 36.0);
            let max = Size::new(inner_max.width + 16.0, inner_max.height + 36.0);

            BoxConstraints::new(min, max)
        };

        let pane = Pane {
            inner: WidgetPod::new(Box::new(widget)),
            title: WidgetPod::new(Label::new(title)),
        };

        let start_bounds = Rect::from_origin_size(Point::new(64.0, 64.0), pane_bc.max());

        self.z_order.push_back(id);
        self.bounds.insert(id, (start_bounds, pane_bc));
        self.panes.insert(id, pane);
    }

    fn get_top_pane_at(&mut self, pos: Point) -> Option<(usize, Rect)> {
        for id in self.z_order.iter().clone() {
            let (bounds, _) = self.bounds.get(id).expect(ERROR_PANE_MISSING_RELATED_VALUE);

            if bounds.contains(pos) {
                return Some((*id, *bounds));
            }
        }

        None
    }
}

impl Widget<ZstState> for Window {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut ZstState, env: &Env) {
        match event {
            Event::MouseDown(e) => {
                // If we are not already dragging/resizing, and the left mouse button is positioned
                // on a pane.
                if self.pane_mut.is_none() && e.button == MouseButton::Left {
                    if let Some((id, bounds)) = self.get_top_pane_at(e.pos) {
                        // We check it's a resize event first

                        // Then we check if it's a drag event
                        let drag_zone = get_pane_drag_zone(bounds);

                        // ... in its drag zone
                        if drag_zone.contains(e.pos) {
                            // ... then we're dragging that pane
                            self.pane_mut = PaneMutation::Dragging(id, e.pos);
                        } else {
                            match get_pane_resize_zone(bounds, e.pos) {
                                Some(dir) => self.pane_mut = PaneMutation::Resizing(id, e.pos, dir),
                                _ => {}
                            }
                        }
                    }
                }
            }
            Event::MouseUp(e) => self.pane_mut = PaneMutation::None,
            Event::MouseMove(e) => match &mut self.pane_mut {
                PaneMutation::Dragging(pane_id, old_pos) => {
                    let (bounds, _bc) = self
                        .bounds
                        .get_mut(&pane_id)
                        .expect(ERROR_PANE_MISSING_RELATED_VALUE);

                    let new_origin = Point {
                        x: bounds.x0 - (old_pos.x - e.pos.x),
                        y: bounds.y0 - (old_pos.y - e.pos.y),
                    };

                    *bounds = bounds.with_origin(new_origin);

                    *old_pos = e.pos;

                    ctx.request_layout()
                }
                PaneMutation::Resizing(pane_id, old_pos, dir) => {
                    let (bounds, bc) = self
                        .bounds
                        .get_mut(&pane_id)
                        .expect(ERROR_PANE_MISSING_RELATED_VALUE);

                    *bounds = resize_from_mouse_delta(*bounds, bc, *old_pos, e.pos, *dir);

                    *old_pos = e.pos;

                    ctx.request_layout()
                }
                _ => {}
            },
            _ => {}
        }

        for id in &mut self.z_order.iter().clone() {
            let pane = self
                .panes
                .get_mut(id)
                .expect(ERROR_ARTEFACT_FROM_DELETED_PANE);

            pane.inner.event(ctx, event, data, env);
            pane.title.event(ctx, event, data, env);
        }
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &ZstState, env: &Env) {
        for (_id, pane) in &mut self.panes {
            pane.inner.lifecycle(ctx, event, data, env);
            pane.title.lifecycle(ctx, event, data, env);
        }
    }

    fn update(&mut self, ctx: &mut UpdateCtx, _old_data: &ZstState, data: &ZstState, env: &Env) {
        for (_id, pane) in &mut self.panes {
            pane.inner.update(ctx, data, env);
            pane.title.update(ctx, data, env);
        }
    }

    fn layout(
        &mut self,
        ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        data: &ZstState,
        env: &Env,
    ) -> Size {
        for (id, pane) in &mut self.panes {
            let (bounds, bc) = self
                .bounds
                .get(id)
                .expect(ERROR_PANE_MISSING_RELATED_VALUE)
                .clone();

            let inner_origin = Point {
                x: bounds.origin().x + PANE_INNER_ORIGIN.x,
                y: bounds.origin().y + PANE_INNER_ORIGIN.y,
            };
            let title_origin = Point {
                x: bounds.origin().x + PANE_TITLE_ORIGIN.x,
                y: bounds.origin().y + PANE_TITLE_ORIGIN.y,
            };

            let inner_bc = {
                let limit = Size::new(bounds.width() - 16.0, bounds.height() - 36.0);
                BoxConstraints::new(bc.min(), bc.constrain(limit))
            };

            pane.inner.layout(ctx, &inner_bc, data, env);
            pane.title.layout(ctx, &bc, data, env);

            pane.inner.set_origin(ctx, data, env, inner_origin);
            pane.title.set_origin(ctx, data, env, title_origin);
        }

        let size = bc.max();

        debug_assert!(!size.width.is_infinite(), "Infinite width passed to Canvas");
        debug_assert!(
            !size.height.is_infinite(),
            "Infinite height passed to Canvas"
        );

        size
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &ZstState, env: &Env) {
        for id in self.z_order.iter().clone() {
            // Save context settings *immediately*
            ctx.save();

            let pane = self
                .panes
                .get_mut(id)
                .expect(ERROR_ARTEFACT_FROM_DELETED_PANE);
            let bounds = self
                .bounds
                .get(id)
                .expect(ERROR_PANE_MISSING_RELATED_VALUE)
                .0;

            ctx.clip(bounds);
            ctx.fill(bounds, &Color::rgb8(64, 101, 107));
            pane.title.paint(ctx, data, env);

            let inner_bounds = get_pane_inner_bounds(bounds);

            ctx.clip(inner_bounds);
            pane.inner.paint(ctx, data, env);

            ctx.restore();
            // Restore context settings for next iteration
        }
    }
}

#[inline]
fn get_pane_inner_bounds(bounds: Rect) -> Rect {
    let x0 = bounds.x0 + 8.0;
    let y0 = bounds.y0 + 28.0;
    let x1 = bounds.x1 - 8.0;
    let y1 = bounds.y1 - 8.0;

    Rect::new(x0, y0, x1, y1)
}

#[inline]
fn get_pane_drag_zone(bounds: Rect) -> Rect {
    let x0 = bounds.x0 + 4.0;
    let y0 = bounds.y0 + 4.0;
    let x1 = bounds.x1 - 4.0;
    let y1 = bounds.y0 + 20.0;

    Rect::new(x0, y0, x1, y1)
}

#[inline]
fn get_pane_resize_zone(bounds: Rect, pos: Point) -> Option<ResizeDir> {
    let outer = bounds.inflate(4.0, 4.0);

    // Starting with the corners

    // Top-left
    let tl = Rect {
        x0: outer.x0,
        y0: outer.y0,
        x1: outer.x0 + 8.0,
        y1: outer.y0 + 8.0,
    };

    if tl.contains(pos) {
        return Some(ResizeDir::TopLeft);
    }

    // Top-right
    let tr = Rect {
        x0: outer.x1 - 8.0,
        y0: outer.y0,
        x1: outer.x1,
        y1: outer.y0 + 8.0,
    };

    if tr.contains(pos) {
        return Some(ResizeDir::TopRight);
    }

    // Bottom-left
    let bl = Rect {
        x0: outer.x0,
        y0: outer.y1 - 8.0,
        x1: outer.x0 + 8.0,
        y1: outer.y1,
    };

    if bl.contains(pos) {
        return Some(ResizeDir::BottomLeft);
    }

    // Bottom-right
    let br = Rect {
        x0: outer.x1 - 8.0,
        y0: outer.y1 - 8.0,
        x1: outer.x1,
        y1: outer.y1,
    };

    if br.contains(pos) {
        return Some(ResizeDir::BottomRight);
    }

    // Now we do the sides. The Rects below will overlap with the corners above, but that is
    // acceptable, since we've already ruled out the corners. There is no overhead to the overlap,
    // but there is a reduction of cognitive complexity to the math, which is desireable.

    // Top
    let t = Rect {
        x0: outer.x0,
        y0: outer.y0,
        x1: outer.x1,
        y1: outer.y0 + 8.0,
    };

    if t.contains(pos) {
        return Some(ResizeDir::Top);
    }

    // Left
    let l = Rect {
        x0: outer.x0,
        y0: outer.y0,
        x1: outer.x0 + 8.0,
        y1: outer.y1,
    };

    if l.contains(pos) {
        return Some(ResizeDir::Left);
    }

    // Right
    let r = Rect {
        x0: outer.x1 - 8.0,
        y0: outer.y0,
        x1: outer.x1,
        y1: outer.y1 + 8.0,
    };

    if r.contains(pos) {
        return Some(ResizeDir::Right);
    }

    // Bottom
    let b = Rect {
        x0: outer.x0,
        y0: outer.y1 - 8.0,
        x1: outer.x1,
        y1: outer.y1,
    };

    if b.contains(pos) {
        return Some(ResizeDir::Bottom);
    }

    None
}

#[inline]
fn resize_from_mouse_delta(mut bounds: Rect, bc: &BoxConstraints, old_pos: Point, pos: Point, dir: ResizeDir) -> Rect {
    let original = bounds;

    let (delta_x, delta_y) = (
        old_pos.x - pos.x,
        old_pos.y - pos.y
    );

    match dir {
        ResizeDir::Top => bounds.y0 -= delta_y,
        ResizeDir::Bottom => bounds.y1 -= delta_y,
        ResizeDir::Left => bounds.x0 -= delta_x,
        ResizeDir::Right => bounds.x1 -= delta_x,
        ResizeDir::TopLeft => {
            bounds.y0 -= delta_y;
            bounds.x0 -= delta_x;
        }
        ResizeDir::TopRight => {
            bounds.y0 -= delta_y;
            bounds.x1 -= delta_x;
        }
        ResizeDir::BottomLeft => {
            bounds.y1 -= delta_y;
            bounds.x0 -= delta_x;
        }
        ResizeDir::BottomRight => {
            bounds.y1 -= delta_y;
            bounds.x1 -= delta_x;
        }
    }

    if !bc.contains(bounds.size()) {
        original
    } else {
        bounds
    }
}

#[derive(Copy, Clone)]
enum ResizeDir {
    Top,
    Bottom,
    Left,
    Right,
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
}
